# LakituAPI #
*An interface between a lakitu pipeline executor and lakitu pipeline*

## Usage ##
The most common expected usage of LakituAPI is for writing new Lakitu pipelines. Looking at example usages in other pipelines is a way to get started. One example is [lpencyclobuildlib](https://bitbucket.org/maccosslab/lpencyclobuildlib). You can also browse the full list of the [MacCoss lab's Lakitu pipelines](https://bitbucket.org/account/user/maccosslab/projects/LKP).

### For writing pipelines - LakituAwsConfig
Available information about cluster resources can be obtained using the LakituAwsConfig:

`from lakituapi.pipeline import LakituAwsConfig`

#### Version 0.1 and up
TODO Add usages of each variable here

#### Version 0.0 and up
TODO Add usages of each variable here

### For writing systems to execute pipelines - PipelineExecutor

For execution, pipeline versions must be less than or equal to that of the lakitu executor to be supported. By default, mismatch between major version of the executor and pipeline will result in an exception being thrown. This can be overridden by setting the LAKITU_API_MAJOR_VERSION_ALLOW_MISMATCH environment variable prior to execution.

## Contributing ##
This project uses the [GitFlow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).

When updating the API, we preserve all previous releases within the module to enable the pipeline executor to be fully backwards compatible with older pipeline releases.  The idea here is that the pipeline runner will sit on some infrastructure that the pipelines need to know some details about in order to run their tasks. For example, submitting jobs to ECS may require that the autoscaling group name be specified in order to request that the cluster be scaled out to accommodate the job.

Additions may be made to this infrastructure to support new types of tasks. For example, if AWS begins supporting Windows on Batch, then a Windows AWS Batch cluster should be added, though we may retain the ECS-only Windows cluster for backwards-compatibility. Another example is that we may wish to support tools written for Apache Spark as part of our pipeline system. These infrastructure additions would warrant new minor-releases. Removal of infrastructure (such as removing the ECS-only Windows cluster) may also be done and would lead to a new major-release, indicating a loss of some backwards-compatibility.

Adding new infrastructure support should only require modifying lakituapi/version.py. New environment variables that will be used by new task types can be added to version.py. Removing infrastructure will require more work, as we may wish to still support old pipelines provided that they only use a supported subset of features.

 When ready to release a new version, create a release branch called release/0.2 (or whatever new version is in this package). Add a tag to match the version in version.py, commit the change, push the branch, build the conda package (conda build conda.recipe) and upload the package (anaconda upload).
