import unittest

import lakituapi
from lakituapi.version import LakituAttributes


class TestVersion(unittest.TestCase):
    """
    Verifies that version import works properly and tests are updated accordingly.
    MAKE SURE TO UPDATE ALL OF THE 'UPDATE ME:' COMMENTS IN VERSION.PY
    """
    def test_version_explicit_string(self):
        self.assertEqual(lakituapi.__version__, "0.1.4")

    def test_attributes_contents(self):
        self.assertSetEqual(set(LakituAttributes['latest'].iteritems()), set(LakituAttributes["{}.{}".format(lakituapi.version.API_MAJOR_VERSION, lakituapi.version.API_MINOR_VERSION)].iteritems()))


if __name__ == '__main__':
    # initialize the test suite
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()

    # add tests to the test suite
    suite.addTest(loader.loadTestsFromTestCase(TestVersion))

    # initialize a runner, pass it your suite and run it
    runner = unittest.TextTestRunner(verbosity=3)
    result = runner.run(suite)
