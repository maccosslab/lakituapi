import ConfigParser
import os
import tempfile
import unittest

import lakituapi
from helpers import CustomTestCase
from lakituapi.pipeline_executor import PipelineExecutor, PipelineExecutorConfig

# Use static external variables so that the variables can be reused between tests, but then pass to member variables
# so that tests don't accidentally rewrite these data structures, which should be constant
GOOD_CONFIG_DICT ={
            'region': 'us-east-1',
            'log_group_name': 'blah',
            'log_group_region': 'us-east-1',
            'windows_cluster_name': 'blah',
            'windows_autoscale_name': 'blah',
            'task_arn': 'blah',
            's3_run_bucket': 'blah-bucket',
            'linux_job_queue_arn': 'blah',
            'stack_name': 'blah',
        }

GOOD_0_0_CONFIG_DICT = {
            'region': 'us-east-1',
            'log_group_name': 'blah',
            'log_group_region': 'us-east-1',
            'windows_cluster_name': 'blah',
            'windows_autoscale_name': 'blah',
            'task_arn': 'blah',
            's3_run_bucket': 'blah',
            'linux_job_queue_arn': 'blah',
        }


class TestPipelineExecutor(CustomTestCase):
    """
    Test cases for the lakituapi.pipeline_executor.PipelineExecutor class
    """

    @staticmethod
    def write_config(version, config_dict):
        """
        Writes a configuration and returns a temporary file pointer to allow for writing of bad configs
        :param version: API version
        :param config_dict:
        :return: temporary file pointer
        """
        config = ConfigParser.SafeConfigParser()
        if version is not None:
            config.add_section('api')
            config.set('api', 'env_major', version[0])
            config.set('api', 'env_minor', version[1])
            config.set('api', 'env_patch', version[2])
        config.add_section('aws')
        for key, value in config_dict.iteritems():
            config.set('aws', key, value)
        f = tempfile.NamedTemporaryFile(mode='w+', delete=False, prefix='cluster', suffix='.cfg', dir=os.getcwd())
        config.write(f)
        return f

    def setUp(self):
        self.good_config_dict = GOOD_CONFIG_DICT
        self.empty_config = {}
        self.good_0_0_config_dict = GOOD_0_0_CONFIG_DICT

        self.good_config_writepointer = TestPipelineExecutor.write_config(version=('0', '1', '4'),
                                                                          config_dict=self.good_config_dict)
        self.good_0_0_config_writepointer = TestPipelineExecutor.write_config(version=('0', '0', '0'),
                                                                              config_dict=self.good_0_0_config_dict)
        self.good_config_no_version_section_writepointer = TestPipelineExecutor.write_config(version=None,
                                                                                             config_dict=self.good_config_dict)
        self.good_config_filename = self.good_config_writepointer.name
        self.good_0_0_config_filename = self.good_0_0_config_writepointer.name
        self.good_config_no_version_section_filename = self.good_config_no_version_section_writepointer.name
        self.good_config_writepointer.close()
        self.good_0_0_config_writepointer.close()
        self.good_config_no_version_section_writepointer.close()

    def tearDown(self):
        pass

    def test_get_variable(self):
        config = ConfigParser.ConfigParser()
        config.read(self.good_config_filename)
        self.assertEqual(config.get('aws', 's3_run_bucket'), 'blah-bucket')

    def test_executor_fails_on_0_0(self):
        config = ConfigParser.ConfigParser()
        config.read(self.good_0_0_config_filename)
        self.assertRaisesRegexp(Exception,
                                "Attempted to load a config version 0\.0\.\d that doesn't match the current"
                                " lakituapi version {}\.{}\.{}".format(lakituapi.version.API_MAJOR_VERSION,
                                                                       lakituapi.version.API_MINOR_VERSION,
                                                                       lakituapi.version.API_PATCH_VERSION),
                                PipelineExecutor,
                                config)

    def test_executor_fails_with_no_version_section(self):
        config = ConfigParser.ConfigParser()
        config.read(self.good_config_no_version_section_filename)
        self.assertRaisesRegexp(Exception,
                                "No section: \'api\'",
                                PipelineExecutor,
                                config)

    def test_executor_succeeds_on_latest(self):
        config = ConfigParser.ConfigParser()
        config.read(self.good_config_filename)
        self.assertNoRaise(Exception, PipelineExecutor, config)

    def test_executor_environment_is_correct(self):
        config = ConfigParser.ConfigParser()
        config.read(self.good_config_filename)
        executor = PipelineExecutor(config)

        expected_environment = {lakituapi.version.LakituAttributes['latest'][key]: value for key, value in self.good_config_dict.iteritems()}
        expected_environment.update({lakituapi.version.LakituAttributes['version']['env_major']: lakituapi.version.API_MAJOR_VERSION})
        expected_environment.update({lakituapi.version.LakituAttributes['version']['env_minor']: lakituapi.version.API_MINOR_VERSION})
        expected_environment.update({lakituapi.version.LakituAttributes['version']['env_patch']: lakituapi.version.API_PATCH_VERSION})

        smaller_set = set(expected_environment)
        larger_set_including_all_env_variables = set(executor.execution_environment)

        # Make sure that all environment variables were set
        self.assertIsSubset(set([value for key, value in lakituapi.version.LakituAttributes['latest'].iteritems()]),
                            larger_set_including_all_env_variables)

        # Make sure that environment variable values are correct
        self.assertIsSubset(smaller_set, larger_set_including_all_env_variables)


class TestPipelineExecutorConfig(CustomTestCase):
    """
    Test cases for the lakituapi.pipeline_executor.PipelineExecutorConfig class
    """

    def setUp(self):
        self.good_config_dict = GOOD_CONFIG_DICT
        self.empty_config = {}
        self.good_0_0_config_dict = GOOD_0_0_CONFIG_DICT

    def test_good_config_passes_validation(self):
        self.assertNoRaiseRegexp(Exception, "All Lakitu parameters must be set", PipelineExecutorConfig.from_dict,
                                 self.good_config_dict)

    def test_empty_config_fails_validation(self):
        self.assertRaisesRegexp(Exception, 'All Lakitu parameters must be set', PipelineExecutorConfig.from_dict,
                                self.empty_config)

    def test_good_0_0_config_dict_fails_validation(self):
        # Test that using new executor with older version fails
        self.assertRaisesRegexp(Exception, 'All Lakitu parameters must be set', PipelineExecutorConfig.from_dict,
                                self.good_0_0_config_dict)

    def test_good_config_has_required_parameters(self):
        self.assertEqual(set(self.good_config_dict.keys()), set(PipelineExecutorConfig.required_parameters()))

    def test_good_config_generates_parsable_config(self):
        config = PipelineExecutorConfig.from_dict(self.good_config_dict)
        self.assertIsInstance(config, ConfigParser.RawConfigParser)

    def test_good_config_write_contains_all_params(self):
        config = PipelineExecutorConfig.from_dict(self.good_config_dict)
        f = tempfile.NamedTemporaryFile(mode='w+', delete=False, prefix='cluster', suffix='.cfg', dir=os.getcwd())
        config.write(f)
        f.close()
        with open(f.name, 'r') as outfile:
            all_expected_config_items = set(self.good_config_dict.keys()).union(set(lakituapi.version.LakituAttributes['version'].keys()))
            larger_set_including_lines_with_junk = set([line.split('=')[0].strip() for line in outfile.readlines()])
            self.assertIsSubset(all_expected_config_items, larger_set_including_lines_with_junk)


if __name__ == '__main__':
    # initialize the test suite
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()

    # add tests to the test suite
    suite.addTest(loader.loadTestsFromTestCase(TestPipelineExecutor))
    suite.addTest(loader.loadTestsFromTestCase(TestPipelineExecutorConfig))

    # initialize a runner, pass it your suite and run it
    runner = unittest.TextTestRunner(verbosity=3)
    result = runner.run(suite)
