import unittest
import re


class CustomTestCase(unittest.TestCase):
    def assertNoRaiseRegexp(self, expected_exception, expected_regex, callable_obj, *args, **kwargs):
        try:
            callable_obj(*args, **kwargs)
        except expected_exception as err:
            if re.compile(expected_regex).search(err.message):
                self.fail("Expected to not see exception of type {} with match to regex {}. Exception message: {}".format(expected_exception, expected_regex, err.message))

    def assertNoRaise(self, expected_exception, callable_obj, *args, **kwargs):
        try:
            callable_obj(*args, **kwargs)
        except expected_exception as err:
            self.fail("Expected to not see exception of type {}. Exception message: {}".format(expected_exception, err.message))

    def assertIsSubset(self, subset, superset):
        self.assertSetEqual(subset - superset, set())  # Should be equivalent to the next test, but gives more readable output
        self.assertTrue(subset.issubset(superset))  # Here for clarity of what we're trying to test
