import os
import unittest

import lakituapi
from helpers import CustomTestCase
from lakituapi.pipeline import LakituAwsConfig
from lakituapi.version import *

CLUSTER_USERNAME = 'dev'


class TestLakituAWSConfigGoodBase(CustomTestCase):
    """
    ABSTRACT CLASS
    """

    @property
    def lakitu_api_major_version(self):
        return str(API_MAJOR_VERSION)

    @property
    def lakitu_api_minor_version(self):
        return str(API_MINOR_VERSION)

    @property
    def lakitu_api_patch_version(self):
        return str(API_PATCH_VERSION)

    def run_environment_initialization(self):
        # set env variables
        _env = os.environ.copy()
        _env["LAKITU_AWS_STACK_NAME"] = 'lakitu-{}'.format(CLUSTER_USERNAME)
        _env["LAKITU_AWS_WIN_CLUSTER"] = 'lakitu-{}-win'.format(CLUSTER_USERNAME)
        _env["LAKITU_AWS_WIN_ASG"] = 'lakitu-{}-ECSWin-blah-ECSAutoScalingGroup-blah'.format(CLUSTER_USERNAME)
        _env["LAKITU_AWS_LOG_GROUP_NAME"] = 'lakitu-{}'.format(CLUSTER_USERNAME)
        _env["LAKITU_AWS_LOG_GROUP_REGION"] = 'us-east-1'
        _env["LAKITU_AWS_S3_RUN_BUCKET"] = 'maccosslab-lakitudata'
        _env["LAKITU_AWS_REGION"] = 'us-east-1'
        _env["LAKITU_AWS_TASK_IAM_ARN"] = 'arn:aws:iam::999145429263:role/lakitu-{}-ECSTaskRole-us-east-1'.format(
            CLUSTER_USERNAME)
        _env["LAKITU_AWS_LINUX_JOB_QUEUE_ARN"] = 'arn:aws:batch:us-east-1:999145429263:job-queue/lakitu-{}-linux-BatchQueue-us-east-1'.format(
            CLUSTER_USERNAME)
        _env["LAKITU_API_MAJOR_VERSION"] = self.lakitu_api_major_version
        _env["LAKITU_API_MINOR_VERSION"] = self.lakitu_api_minor_version
        _env["LAKITU_API_PATCH_VERSION"] = self.lakitu_api_patch_version
        os.environ.update(_env)

    def run_post_environment_initialization(self):
        reload(lakituapi)
        reload(lakituapi.pipeline)
        globals()['LakituAwsConfig'] = lakituapi.pipeline.LakituAwsConfigLoader()

    def setUp(self):
        self.old_env = os.environ.copy()
        self.run_environment_initialization()
        self.run_post_environment_initialization()

    def tearDown(self):
        os.environ.update(self.old_env)


class TestLakituAWSConfig(TestLakituAWSConfigGoodBase):
    """
    Tests the behavior of LakituAWSConfig. The environment variable LAKITU_TEST_CLUSTER_USERNAME must be set to match
     the before
    running tests.
    """

    def test_get_stack_name(self):
        self.assertEqual(LakituAwsConfig.stack_name, 'lakitu-{}'.format(CLUSTER_USERNAME))

    def test_get_win_autoscale_name(self):
        # This assumes a deployed cluster
        windows_autoscale_name = LakituAwsConfig.windows_autoscale_name
        self.assertRegexpMatches(windows_autoscale_name, '^lakitu-{}-ECSWin-.*-ECSAutoScalingGroup-.*$'.format(CLUSTER_USERNAME))

    def test_correct_version(self):
        self.assertEqual(LakituAwsConfig.env_major, '0')
        self.assertEqual(LakituAwsConfig.env_minor, '1')
        self.assertEqual(LakituAwsConfig.env_patch, '4')


class TestLakituAWSConfigNewerMinorVersion(TestLakituAWSConfigGoodBase):
    """
    Tests the behavior of LakituAWSConfig. The environment variable LAKITU_TEST_CLUSTER_USERNAME must be set to match
     the before
    running tests.
    """

    def run_post_environment_initialization(self):
        pass

    @property
    def lakitu_api_minor_version(self):
        return str(API_MINOR_VERSION - 1)

    def test_initialize_failure_for_newer_minor_version(self):
        def do_reload():
            reload(lakituapi)
            reload(lakituapi.pipeline)
            globals()['LakituAwsConfig'] = lakituapi.pipeline.LakituAwsConfigLoader()
        self.assertRaisesRegexp(Exception,
                                "The Lakitu version used to call this task {}\.{}\.{} is less"
                                " than required by the task {}\.{}\.{}\. If you are using lakitu"
                                " on the command line\, you will need to upgrade it to"
                                " run this pipeline".format(API_MAJOR_VERSION, API_MINOR_VERSION - 1, API_PATCH_VERSION, API_MAJOR_VERSION, API_MINOR_VERSION, API_PATCH_VERSION),
                                do_reload)


class TestLakituAWSConfigOlderMinorVersion(TestLakituAWSConfigGoodBase):
    """
    Tests the behavior of LakituAWSConfig. The environment variable LAKITU_TEST_CLUSTER_USERNAME must be set to match
     the before
    running tests.
    """

    def run_post_environment_initialization(self):
        pass

    @property
    def lakitu_api_minor_version(self):
        return str(API_MINOR_VERSION + 1)

    def test_initialize_failure_for_older_minor_version(self):
        def do_reload():
            reload(lakituapi)
            reload(lakituapi.pipeline)
            globals()['LakituAwsConfig'] = lakituapi.pipeline.LakituAwsConfigLoader()

        self.assertNoRaiseRegexp(Exception,
                                 "The Lakitu version used to call this task {}\.{}\.{} is less"
                                 " than required by the task {}\.{}\.{}\. If you are using lakitu"
                                 " on the command line\, you will need to upgrade it to"
                                 " run this pipeline".format(API_MAJOR_VERSION, API_MINOR_VERSION - 1, API_PATCH_VERSION, API_MAJOR_VERSION, API_MINOR_VERSION, API_PATCH_VERSION),
                                 do_reload)


class TestLakituAWSConfigOlderMajorVersion(TestLakituAWSConfigGoodBase):
    """
    Tests the behavior of LakituAWSConfig. The environment variable LAKITU_TEST_CLUSTER_USERNAME must be set to match
     the before
    running tests.
    """

    def run_post_environment_initialization(self):
        pass

    @property
    def lakitu_api_major_version(self):
        return str(API_MAJOR_VERSION + 1)

    def test_initialize_failure_for_older_major_version(self):
        def do_reload():
            reload(lakituapi)
            reload(lakituapi.pipeline)
            globals()['LakituAwsConfig'] = lakituapi.pipeline.LakituAwsConfigLoader()
        self.assertRaisesRegexp(Exception,
                                "The Lakitu major version used to call this task \({}\) is not the same as that of the task \({}\),"
                                " indicating that forward-compatibility is not supported.".format(self.lakitu_api_major_version, API_MAJOR_VERSION),
                                do_reload)


class TestEmptyLakituAWSConfig(unittest.TestCase):
    """
    Tests the behavior of an empty LakituAWSConfig object
    """

    def setUp(self):
        self.old_env = os.environ.copy()
        os.environ.pop("LAKITU_AWS_STACK_NAME", None)
        os.environ.pop("LAKITU_AWS_WIN_CLUSTER", None)
        os.environ.pop("LAKITU_AWS_WIN_ASG", None)
        os.environ.pop("LAKITU_AWS_LOG_GROUP_NAME", None)
        os.environ.pop("LAKITU_AWS_LOG_GROUP_REGION", None)
        os.environ.pop("LAKITU_AWS_S3_RUN_BUCKET", None)
        os.environ.pop("LAKITU_AWS_REGION", None)
        os.environ.pop("LAKITU_AWS_TASK_IAM_ARN", None)
        os.environ.pop("LAKITU_AWS_LINUX_JOB_QUEUE_ARN", None)
        os.environ.pop("LAKITU_API_MAJOR_VERSION", None)
        os.environ.pop("LAKITU_API_MINOR_VERSION", None)
        os.environ.pop("LAKITU_API_PATCH_VERSION", None)
        reload(lakituapi)
        reload(lakituapi.pipeline)
        globals()['LakituAwsConfig'] = lakituapi.pipeline.LakituAwsConfigLoader()

    def tearDown(self):
        os.environ.update(self.old_env)

    def test_get_all(self):
        # Test that an empty LakituAWSConfig returns none for all properties and throws no errors
        self.assertIsNone(LakituAwsConfig.env_major)
        self.assertIsNone(LakituAwsConfig.env_minor)
        self.assertIsNone(LakituAwsConfig.region)
        self.assertIsNone(LakituAwsConfig.stack_name)
        self.assertIsNone(LakituAwsConfig.log_group_name)
        self.assertIsNone(LakituAwsConfig.log_group_region)
        self.assertIsNone(LakituAwsConfig.windows_cluster_name)
        self.assertIsNone(LakituAwsConfig.task_arn)
        self.assertIsNone(LakituAwsConfig.s3_run_bucket)
        self.assertIsNone(LakituAwsConfig.linux_job_queue_arn)
        self.assertIsNone(LakituAwsConfig.windows_autoscale_name)


if __name__ == '__main__':
    # initialize the test suite
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()

    # add tests to the test suite
    suite.addTest(loader.loadTestsFromTestCase(TestLakituAWSConfig))
    suite.addTest(loader.loadTestsFromTestCase(TestLakituAWSConfigNewerMinorVersion))
    suite.addTest(loader.loadTestsFromTestCase(TestLakituAWSConfigOlderMajorVersion))
    suite.addTest(loader.loadTestsFromTestCase(TestLakituAWSConfigOlderMinorVersion))
    suite.addTest(loader.loadTestsFromTestCase(TestEmptyLakituAWSConfig))

    # initialize a runner, pass it your suite and run it
    runner = unittest.TextTestRunner(verbosity=3)
    result = runner.run(suite)
