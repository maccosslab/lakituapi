"""
pipeline.py
Functions for Lakitu Pipelines --
Lakitu pipelines can use these classes and functions to read information provided to them in the
 system environment by the lakitu executor. Luigi tasks can import the LakituAwsConfig singleton to get the AWS
 configuration that has been passed in via environment variables.
"""
import os

from lakituapi.version import API_VERSION, API_MAJOR_VERSION, LakituAttributes


class LakituAwsConfigLoader(object):

    def __init__(self):
        # Initialize all attributes to None. We set attributes, like self.env_major, self.region, etc. dynamically
        #  from the LakituAttributes structure
        for key, env_var in LakituAttributes['latest'].iteritems():
            setattr(self, key, os.environ.get(env_var))

        # Pull only the version variables from the environment
        for key, env_var in LakituAttributes['version'].iteritems():
            setattr(self, key, os.environ.get(env_var))

        if self.env_major is not None and self.env_minor is not None and self.env_patch is not None:  # Allow for empty initialization for build testing...
            env_version = (int(self.env_major), int(self.env_minor), int(self.env_patch))
            if env_version < API_VERSION:
                raise Exception("The Lakitu version used to call this task {}.{}.{} is less"
                                " than required by the task {}.{}.{}. If you are using lakitu"
                                " on the command line, you will need to upgrade it to"
                                " run this pipeline".format(env_version[0], env_version[1], env_version[2], API_VERSION[0], API_VERSION[1], API_VERSION[2]))

            if int(self.env_major) != API_MAJOR_VERSION and os.environ.get('LAKITU_API_MAJOR_VERSION_ALLOW_MISMATCH') is None:
                raise Exception("The Lakitu major version used to call this task ({}) is not the same as that of the"
                                " task ({}), indicating that forward-compatibility is not supported.".format(self.env_major, API_MAJOR_VERSION))

            # Only set all attributes when validity checks are passed
            for key, env_var in LakituAttributes['latest'].iteritems():
                setattr(self, key, os.environ.get(env_var))


LakituAwsConfig = LakituAwsConfigLoader()
