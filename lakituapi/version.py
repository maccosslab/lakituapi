# UPDATE ME (if necessary): Increment when infrastructure and its environment variables are changed/removed. This
#  means a breaking change! No backwards compatibility! Only use if absolutely necessary!
API_MAJOR_VERSION = 0

# UPDATE ME: Increment when infrastructure is added. E.g. adding a Spark cluster, Hadoop, etc.
API_MINOR_VERSION = 1

API_PATCH_VERSION = 4


API_VERSION = (API_MAJOR_VERSION, API_MINOR_VERSION, API_PATCH_VERSION)

LakituAttributes = {
    'version': {
        'env_major': 'LAKITU_API_MAJOR_VERSION',
        'env_minor': 'LAKITU_API_MINOR_VERSION',
        'env_patch': 'LAKITU_API_PATCH_VERSION',
    },
    '0.0': {
        'region': 'LAKITU_AWS_REGION',
        'log_group_name': 'LAKITU_AWS_LOG_GROUP_NAME',
        'log_group_region': 'LAKITU_AWS_LOG_GROUP_REGION',
        'windows_cluster_name': 'LAKITU_AWS_WIN_CLUSTER',
        'windows_autoscale_name': 'LAKITU_AWS_WIN_ASG',
        'task_arn': 'LAKITU_AWS_TASK_IAM_ARN',
        's3_run_bucket': 'LAKITU_AWS_S3_RUN_BUCKET',
        'linux_job_queue_arn': 'LAKITU_AWS_LINUX_JOB_QUEUE_ARN',
    },
    '0.1': {
        'stack_name': 'LAKITU_AWS_STACK_NAME',
    },
    # UPDATE ME: Add new attribute dictionary here, for example:
    #   v0_2_attributes = {'something': 'LAKITU_SOMETHING'}
}
LakituAttributes['0.0'].update(LakituAttributes['version'])
LakituAttributes['0.1'].update(LakituAttributes['0.0'])
# UPDATE ME: Add new attribute update here, for example:
#   LakituAttributes['0.2'].update(LakituAttributes['0.1'])

LakituAttributes['latest'] = LakituAttributes['{}.{}'.format(API_MAJOR_VERSION, API_MINOR_VERSION)]
