"""
pipeline_executor.py

Functions for Lakitu Executors --
Lakitu executors must set up an execution environment for a lakitu pipeline. Environment variables are used to pass
information about the infrastructure that is available to run tasks.
"""

import ConfigParser
import os

from lakituapi.version import API_MAJOR_VERSION, API_MINOR_VERSION, API_PATCH_VERSION, LakituAttributes


class PipelineExecutor(object):
    def __init__(self, config):
        config_version = (config.get('api', 'env_major'), config.get('api', 'env_minor'), config.get('api', 'env_patch'))
        executor_version = (str(API_MAJOR_VERSION), str(API_MINOR_VERSION), str(API_PATCH_VERSION))
        if config_version != executor_version:
            raise Exception("Attempted to load a config version {}.{}.{} that doesn't match the current lakituapi version {}.{}.{}".format(*(list(config_version) + list(executor_version))))
        self._env = os.environ.copy()
        self._env["LAKITU_API_MAJOR_VERSION"] = str(API_MAJOR_VERSION)
        self._env["LAKITU_API_MINOR_VERSION"] = str(API_MINOR_VERSION)
        self._env["LAKITU_API_PATCH_VERSION"] = str(API_PATCH_VERSION)
        for key, env_var in LakituAttributes['latest'].iteritems():
            section = env_var.split('_')[1].lower()  # Format of environment variables is LAKITU_<SECTION>_REST_OF_VAR
            self._env[env_var] = config.get(section, key)
        try:
            self._env["AWS_SHARED_CREDENTIALS_FILE"] = os.path.expanduser(config.get('aws', 'credentials_file'))
        except ConfigParser.NoOptionError:
            """
            boto3 may also get shared credentials by an IAM role, so we will allow execution to continue even if a
             shared credentials file is not specified. IAM roles should be used whenever possible to limit access to
             AWS credentials.
             See https://boto3.amazonaws.com/v1/documentation/api/latest/guide/configuration.html#best-practices-for-configuring-credentials
            """
            pass

    @property
    def execution_environment(self):
        return self._env


class PipelineExecutorConfig(object):
    """
    A class that takes a dict defining the environment variables, validates that all are present for the latest
    API version, and generates a config file from them.
    """

    @staticmethod
    def required_parameters():
        """
        These are the parameters required to be set by the set_parameters() function
        """
        # The version is excluded because the config writer injects the running version of lakituapi into the config
        return list(set(LakituAttributes['latest'].keys()) - set(LakituAttributes['version'].keys()))

    @staticmethod
    def from_dict(config_dict):
        """
        Generate a ConfigParser object that can be written to file using the .write() method, for example:
        ```
        config_params = {'stack_name': 'lakitu-yourstackname', 'region': 'us-east-1', ...}
        config = PipelineExecutorConfig.from_dict(config_params)
        config.write(open('lakitu.cfg', 'w')
        ```
        :param config_dict: Dictionary of lakitu parameters and values. Must contain all parameters provided by the required_parameters() method
        :return:
        """
        PipelineExecutorConfig.validate(config_dict)
        config = ConfigParser.SafeConfigParser()
        config.add_section('api')
        config.set('api', 'env_major', str(API_MAJOR_VERSION))
        config.set('api', 'env_minor', str(API_MINOR_VERSION))
        config.set('api', 'env_patch', str(API_PATCH_VERSION))
        config.add_section('aws')
        for key, value in config_dict.iteritems():
            config.set('aws', key, value)
        return config

    @staticmethod
    def validate(config_dict):
        if set(PipelineExecutorConfig.required_parameters()) != set(config_dict):
            raise Exception("All Lakitu parameters must be set")
