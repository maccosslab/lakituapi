import version
__version__ = "{}.{}.{}".format(version.API_MAJOR_VERSION, version.API_MINOR_VERSION, version.API_PATCH_VERSION)
